import java.util.ArrayList;

public class Percolation 
{
	private final int N;
	private final int VIRTUAL_SITE_HI;
	private final int VIRTUAL_SITE_LO;
	private UF uf;
	
	private ArrayList <Integer> openSites;
		
	// Create N-by-N Grid with all spaces blocked
	public Percolation(int N)
	{
		this.N = N;
		uf = new UF(N * N + 2); 		// Gives space for Virtual sites
		VIRTUAL_SITE_HI = 0;
		VIRTUAL_SITE_LO = N * N + 1;
		openSites = new ArrayList<Integer>();
	}
	
	// Open site (row i, column j) if it is not already
	public void open(int i, int j)
	{			
		int site = N * (i - 1) + j;
		
		
		// automatically connects bottom row to virtual site
		if (i == N)
		{
			uf.union(site, VIRTUAL_SITE_LO);
		}

		// Automatically connects all of the top row to the top virtual site
		if (i == 1)
		{
			uf.union(site, VIRTUAL_SITE_HI);
		}
		
		// Connects sites to the one above it
		if (isOpen(i - 1, j))
		{
			uf.union(site, site - N);
		}
		
		// connects sites to the one below it
		if (isOpen(i + 1, j))
		{
			uf.union(site, site + N);
		}
		
		// connects to the one to the right
		if (isOpen(i, j + 1) && j != N)
		{
			uf.union(site, site + 1);
		}
		
		// connects to the one to the left
		if (isOpen(i, j - 1) && j != 1)
		{
			uf.union(site, site - 1);
		}
		
		openSites.add(site);
	}
	
	// checks if site i, j is open
	public boolean isOpen(int i, int j)
	{
		int site = N * (i - 1) + j;
		
		return openSites.contains(site);
	}
	
	// checks if site i, j is full
	public boolean isFull(int i, int j)
	{
		int site = N * (i - 1) + j;
				
		return uf.connected(site, VIRTUAL_SITE_HI);
	}
	
	// checks if the system percolates
	public boolean percolates()
	{
		return uf.connected(VIRTUAL_SITE_LO, VIRTUAL_SITE_HI);
	}
}
