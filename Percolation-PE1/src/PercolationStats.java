public class PercolationStats 
{
	private int T;
	private int N;
	private Percolation[] percs;
	private double[] sums;
	// perform T independent computational experiments on an N-by-N grid
	public PercolationStats(int N, int T)
	{
		this.T = T;
		this.N = N;
		percs = new Percolation[T];
		sums = new double[T];
	}
	
	// sample mean of percolation threshold
	public double mean()
	{
		return StdStats.mean(sums);
	}
	
	// sample standard deviation of percolation threshold
	public double stddev()
	{
		if (T == 1)
		{
			return Double.NaN;
		}
		return StdStats.stddev(sums);
	}
	
	// returns lower bound of the 95% confidence interval
	public double confidenceLo()
	{
		return mean() - ((1.96 * stddev()) / Math.sqrt(T));
	}
	
	// returns upper bound of the 95% confidence interval
	public double confidenceHi()
	{
		return mean() + ((1.96 * stddev()) / Math.sqrt(T));
	}
	
	public static void main(String[] args)
	{
		 In in = new In(args[0]);      // input file
	     int N = in.readInt();         // N-by-N percolation system
	     int T = in.readInt();
	     	     
	     PercolationStats percStats = new PercolationStats(N, T);
	     for (int i = 0; i < T; i++)
	     {
	    	 percStats.percs[i] = new Percolation(N);
    	     double sum = 0;
    	     
	    	 while (!percStats.percs[i].percolates())
	    	 { 
		    	int p = StdRandom.uniform(N) + 1;
		    	int j = StdRandom.uniform(N) + 1; 
		    		 
	    		 if (!percStats.percs[i].isOpen(p, j))
	    		 {
		    		 percStats.percs[i].open(p, j);
	    			 sum++;
	    		 }
	    	 }
	    	 
	    	 percStats.sums[i] = sum / (N * N);
	     }
	     System.out.println("mean			= " + percStats.mean());
	     System.out.println("stddev			= " + percStats.stddev());
	     System.out.println("95% confidence interval = " + percStats.confidenceLo() + ", " + percStats.confidenceHi());
	}
}